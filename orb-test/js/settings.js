jQuery(document).ready(function(){
	jQuery('.btn-send').click(function()
	{
		full_name = jQuery('#full_name').val();
		name_regex = /^[a-zA-Z ']+$/;
		if( !full_name.match(name_regex) || full_name.length < 3 || full_name.trim() =='')
		{
			jQuery('#full_name').addClass('error');
			jQuery('#full_name').val('').attr('placeholder','Please enter valid full name');
		}
		else
		{
			jQuery('#full_name').removeClass('error');
			jQuery.ajax({
				url: js_var.admin_url,
				method: 'POST',
				data: {
					action: 'api_call',
					sec_nonce: js_var.valid_nonce,
					full_name:full_name
				},
				success: function(data){
					jQuery('.response-dv').html(data).slideDown();
				},
				error: function(err){
					alert('Something went wrong with AJAX call. Please try again!');
				}
			});
		}
	});
});