<?php
/*
* Plugin Name: Assignment plugin for Orbisius
* Plugin URI:https://gitlab.com/aparnascodex/assignment
* Author: Aparna
* Author URI: http://aparnascodex.com
* Version: 1.5
* Description: Creates submenu "API settings" under Settings Menu. It captures input from user, sends it to API and displays API response.
*/

/*
* Hook : admin_menu
* Description: Create submenu 'API settings' under Settings Menu
*/
add_action('admin_menu', 'add_api_settings_page');
function add_api_settings_page()
{
	add_submenu_page( 'options-general.php', 'API Settings', 'API Settings', 'manage_options', 'api_settings', 'api_inegration_settings_page' );
}

/*
* api_inegration_settings_page - function definition
*/
function api_inegration_settings_page()
{
	wp_enqueue_style('settings-css',plugins_url('css/settings.css',__FILE__));
	wp_enqueue_script('settings-js',plugins_url('js/settings.js',__FILE__),array('jquery'));
	wp_localize_script('settings-js','js_var',array('admin_url' => admin_url('admin-ajax.php'),
											   		'valid_nonce' => wp_create_nonce('validate_req')));
	?>

	<div class='wrap'>
			<h2>Settings</h2>
		<div class='inner-content'>
			<div class='fields-dv'>
				<label> Full Name: </label>
				<input type='text' name='full_name' id='full_name'>
				<input type='button' class='btn-send button-primary button' value='Send'>
			</div>
			<div class='response-dv'>
			</div>
		</div>
	</div>

	<?php
}

/*
* Hook : wp_ajax
* Description : Call API after nonce is validated.
*/
add_action('wp_ajax_api_call','api_call');
function api_call()
{
	check_ajax_referer('validate_req', 'sec_nonce');
	$full_name = $_POST['full_name'];

	//Curl request to get API response
	$url = 'https://orbisius.com/apps/qs_cmd/?json';
	$postData = array(
		  			'full_name' => $full_name		  
				);

	$handle = curl_init();
	curl_setopt($handle, CURLOPT_URL, $url);
	curl_setopt($handle, CURLOPT_POST, true);	    
	curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($handle, CURLOPT_POSTFIELDS, $postData);
	curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

	
	$data = curl_exec($handle);
	curl_close($handle);
	//Check if there is any error in request
	if (curl_error($handle)) {
   		echo $error_msg = curl_error($handle);
	}
	else
	{
		//Process and display response
		$response = json_decode($data);
		$result = '<h3> Response:</h3>
					<ul>';
		foreach($response as $key=>$value)
			$result .= '<li><label>'.$key.':</label> '.$value.'</li>';
		$result .= '</ul>';
		echo $result;
	}	

	die();
}
