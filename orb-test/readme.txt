=== Assignment plugin for Orbisius ===

Contributors: aparnascodex
Tested up to: 5.2.2

Assignment plugin 

== Description ==

Creates submenu "API settings" under Settings Menu. It captures input from user, sends it to API and displays API response.

== Installation ==

1. Download the plugin orb-test.zip
2. Go to WordPress Dashboard. Locate Plugins -> Add New
3. Click on Upload Plugin link from top
4. Upload the downloaded orb-test.zip file and click on Install Now
5. After installation, click on Activate Plugin link to activate the plugin.

== Changelog ==

= 1.0 =

Done with the development